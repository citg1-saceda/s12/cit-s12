package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    //    Create
    void createUser(User user);

    //    View all
    Iterable<User> getUsers();

    //    Delete
    ResponseEntity deleteUser(Long id);

    //    Update
    ResponseEntity updateUser(Long id, User user);
}
