package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    //    Create
    public void createUser(User user){
        userRepository.save(user);
    }

    //    Get All
    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

    //  Delete
    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("User Deleted Successfully.", HttpStatus.OK);
    }

    //  Update
    public ResponseEntity updateUser(Long id, User user){

        User userForUpdate = userRepository.findById(id).get();

        userForUpdate.setUsername(user.getUsername());
        userForUpdate.setPassword(user.getPassword());

        userRepository.save(userForUpdate);

        return new ResponseEntity<>("User updated successfully", HttpStatus.OK);
    }
}
